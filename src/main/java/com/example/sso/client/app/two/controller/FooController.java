package com.example.sso.client.app.two.controller;

import com.example.sso.client.app.two.model.Foo;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class FooController {

    @Value("${resource-server.api.foos.url}")
    private String foosApiUrl;

    private final WebClient webClient;

    @GetMapping("/foos")
    public String getFoos(Model model) {
        List<Foo> foos = this.webClient.get()
                .uri(foosApiUrl)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<Foo>>() {
                })
                .block();
        model.addAttribute("foos", foos);
        return "foos";
    }
}
