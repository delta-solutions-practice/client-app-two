package com.example.sso.client.app.two.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class Foo {

    @NonNull
    private Long id;

    @NonNull
    private String name;
}
