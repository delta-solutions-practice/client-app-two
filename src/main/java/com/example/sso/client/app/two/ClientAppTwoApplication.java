package com.example.sso.client.app.two;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientAppTwoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientAppTwoApplication.class, args);
    }

}
